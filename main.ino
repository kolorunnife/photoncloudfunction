int redLed = D0;
int yellowLed = D1;
int greenLed = D2;

bool redOn = false, yellowOn = false, greenOn = false;

int toggleLed(String led) {
    if (led == "red") {
        digitalWrite(redLed, !redOn);
        redOn = !redOn;
        return 0;
    }
    else if (led == "yellow") {
        digitalWrite(yellowLed, !yellowOn);
        yellowOn = !yellowOn;
        return 1;
    }
    else if (led == "green") {
        digitalWrite(greenLed, !greenOn);
        greenOn = !greenOn;
        return 2;
    }
    else {
        return -1;
    }
}

void setup() {
    pinMode(redLed, OUTPUT);
    pinMode(yellowLed, OUTPUT);
    pinMode(greenLed, OUTPUT);
    
    Particle.function("toggleLed", toggleLed);
}

void loop() {

}